const usersController = require('./users-controller');
const commentsController = require('./comments-controller');
const postsController = require('./posts-controller');

module.exports = {
    usersController,
    commentsController,
    postsController,
};
