const usersRepository = require('./users-repo');
const commentsRepository = require('./comments-repo');
const imagesRepository = require('./images-repo');
const postsRepository = require('./posts-repo');

module.exports = {
    usersRepository,
    commentsRepository,
    imagesRepository,
    postsRepository,
};
